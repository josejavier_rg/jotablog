const URLBASE = "https://api.mlab.com/api/1/databases/bootcampjass/collections/postsjaas"
const APIKEY = "?apiKey=SGJFX_7hANA-eUprGG1mWSnwtPBPLecI"

var response;

function obtenerPosts() {
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLBASE+APIKEY, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  //mostrarPosts();
};

function mostrarPosts() {
  var tabla = document.getElementById("tablaPosts")
  for (var i = 0; i < response.length; i++) {
  //alert(response[i].titulo);
  var fila = tabla.insertRow(i+1);
  var celdaId = fila.insertCell(0);
  var celdaTitulo = fila.insertCell(1);
  var celdaTexto = fila.insertCell(2);
  var celdaAutor = fila.insertCell(3);
  var celdaOperaciones = fila.insertCell(4);

  celdaId.innerHTML = response[i]._id.$oid;
  celdaTitulo.innerHTML = response[i].titulo;
  celdaTexto.innerHTML = response[i].texto;
  if (response[i].autor != undefined)
  {
    celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
  }
  else
  {
    celdaAutor.innerHTML = "Anónimo";
  }
  celdaOperaciones.innerHTML = '<button onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>'+
  '<button onclick=\'borrarPost("' + celdaId.innerHTML + '")\';>Borrar</button>';
  }
};

function anadirPost() {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URLBASE+APIKEY, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Nuevo POST desde Atom", "texto":"Nuevo texto desde ATOM", "autor":{"nombre":"Jose Javier","apellido":"Rodriguez"}}');
};

function actualizarPost(id){
  var peticion = new XMLHttpRequest();
  peticion.open("PUT", URLBASE+'/'+id+APIKEY, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Titulo cambiado5"}');
};

function borrarPost(id){
  var peticion = new XMLHttpRequest();
  peticion.open("DELETE", URLBASE+'/'+id+APIKEY, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
};

function seleccionarPost(numero){
  sessionStorage["seleccionado"]=numero;
};

function buscarDetallesPost(numero){
  var posts = JSON.parse(sessionStorage["posts"]);
    for (var i = 0; i < response.length; i++) {
      if (posts[i].id.$oid ==numero)
      {
        document.getElementById("h1").innerHTML = numero;
        document.getElementById("h2").innerHTML = posts[i].titulo;
        document.getElementById("h3").innerHTML = posts[i].texto;
        break;
      }
    }
};
