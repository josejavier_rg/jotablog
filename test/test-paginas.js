var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe("Pruebas sencillas", function() {
  it('Test suma', function() {
    expect(9+4).to.equal(13);
  });
});

describe("Pruebas de red", function() {
  it('Test internet', function (done) {
    request.get("http://www.as.com",
    function (error, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('Test acceso a mi blog', function (done) {
    request.get("http://localhost:8082",
    function (error, response, body) {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('Test mi blog contiene bienvenida', function (done) {
    request.get("http://localhost:8082",
    function (error, response, body) {
      expect(body).contains('Bienvenido a mi blog');
      done();
    });
  });
});

describe("Test contenio html", function(done) {
  it('Test H1', function(done) {
    request.get("http://localhost:8082",
    function (error, response, body) {
      expect($('body h1')).to.have.text('Pepe');
          done();
    });
    done();
  });
});
