'user strict';

const express = require('express');
const path = require('path');

// Constantes
const PORT = 8081;

// Aplicacion
const app = express();

app.use(express.static(__dirname));

app.get('/', function(req, res) {
  //res.send("Bienvenido al blog\n");
  res.sendFile(path.join(__dirname+'/index.html'));
});

//app.get('/detallePost/:id', function(req, res) {
//  res.sendFile(path.join(__dirname+'/detallePost.html'));
//});

app.get('/detallePost/:id', function(req, res) {
  res.sendFile(path.join(__dirname+'/admin.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto" + PORT);
